// 1)select the add to cart field

let addToCartField = document.querySelectorAll('.add-to-cart-field');
if(addToCartField)
{
	// for plus and minus quantity limit checker
	function isMinLimit(input)
	{
		if(input.value <= 0){
			return true;
		}else{
			return false;
		}
	}


	function isMaxLimit(input){
		if(input.value >=99){
			return true;
		}else{
			return false;
		}
	}

	let deductQuantityBtns = document.querySelectorAll('.deduct-quantity')
	deductQuantityBtns.forEach(function(deductQuantityBtn)
	{

		let deductQuantityBtnDataID=deductQuantityBtn.dataset.id;
		console.log(deductQuantityBtnDataID);

		let inputTarget = document.querySelector('input[data-id="' +deductQuantityBtnDataID+'"]');

		deductQuantityBtn.addEventListener('click',function(){
			// console.log(inputTarget);

			if(!isMinLimit(inputTarget)){
				inputTarget.value--;
			}
			else{
				inputTarget.value = 0;
			}
		});
	});
	let addQuantityBtns = document.querySelectorAll('.add-quantity');
	addQuantityBtns.forEach(function(addQuantityBtn)
	{
				console.log(addQuantityBtn);

		let addQuantityDataId = addQuantityBtn.dataset.id;

		// select the input Target
		let inputTarget = document.querySelector(`input[data-id="${addQuantityDataId}"]`)

		addQuantityBtn.addEventListener('click',()=>{

			if(!isMaxLimit(inputTarget)){
				inputTarget.value++;
			}
			else{
				inputTarget.value = 99;
			}
			
		});

	});

	// For input fields
    	let inputQuantities = document.querySelectorAll('.input-quantity');
		console.log(inputQuantities);
		inputQuantities.forEach((inputQuantity)=> {
			inputQuantity.addEventListener('input',function(event){
				console.log(event);
				if(isMinLimit(inputQuantity)){
					inputQuantity.value =0;
				}
				if (isMaxLimit(inputQuantity)) {
					inputQuantity.value = 99;
				}
			});

			// Select all input on click
			inputQuantity.addEventListener('click', ()=> {
				inputQuantity.select();
			})
		});

}

