-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 30, 2019 at 10:00 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clothing`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Women', NULL, NULL),
(2, 'Men', NULL, NULL),
(3, 'Kids', NULL, NULL),
(4, 'teens', '2019-10-27 16:52:04', '2019-10-27 16:52:04'),
(5, 'Girls', '2019-10-27 17:08:58', '2019-10-27 17:08:58'),
(6, 'babies', '2019-10-27 17:21:59', '2019-10-27 17:21:59'),
(7, 'Boys', '2019-10-27 17:54:03', '2019-10-27 17:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_24_061135_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_10_24_061451_create_categories_table', 1),
(6, '2019_10_24_061932_create_products_table', 1),
(7, '2019_10_24_062007_create_statuses_table', 1),
(8, '2019_10_24_062136_create_payment_modes_table', 1),
(9, '2019_10_24_062229_create_transactions_table', 1),
(10, '2019_10_24_062430_create_transaction_products_table', 1),
(11, '2019_10_28_015627_update_categories_table', 2),
(12, '2019_10_29_045730_update_transactions_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE `payment_modes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'cod', NULL, NULL),
(2, 'paypal', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `image`, `category_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'dress', 804343.00, 'dfgfg', 'images/2019-10-25-04-36-44_izmAvLigEz_blouse.jpeg', 1, '2019-10-25 00:12:56', '2019-10-24 20:36:44', '2019-10-25 00:12:56'),
(2, 'blouse', 87.00, 'floral top short sleeve', 'images/2019-10-25-07-56-55_SXqsjdeJ3V_men_tshirt.webp', 1, '2019-10-25 00:10:21', '2019-10-24 20:45:41', '2019-10-25 00:10:21'),
(3, 'shirt', 140.00, 'white shirt cotton', 'images/2019-10-25-06-18-58_v2TO5yxeSk_whiteshirt.jpeg', NULL, '2019-10-25 00:02:03', '2019-10-24 22:18:58', '2019-10-25 00:02:03'),
(7, 'shirt', 200.00, 'white shirt', 'images/2019-10-25-08-12-24_DjBxWv41Ce_whiteshirt.jpeg', 2, NULL, '2019-10-25 00:12:24', '2019-10-25 00:12:24'),
(8, 'short blouse', 50.00, 'flloral', 'images/2019-10-25-08-14-20_RHgE6mHx5I_blouse.jpeg', 1, '2019-10-25 00:15:09', '2019-10-25 00:14:20', '2019-10-25 00:15:09'),
(9, 'test', 122.00, 'baby', 'images/2019-10-25-08-16-26_9TnotNKPYi_jammies.jpeg', 3, '2019-10-25 00:16:48', '2019-10-25 00:16:26', '2019-10-25 00:16:48'),
(10, 'Onesie', 30.00, 'bodysuit unisex', 'images/2019-10-28-00-28-18_7FqypS9Tmk_jammies.jpeg', 1, '2019-10-27 16:28:37', '2019-10-27 16:28:18', '2019-10-27 16:28:37'),
(11, 'Onesie', 30.00, '3pack onesie unisex', 'images/2019-10-28-00-29-15_Jzv0p48sMo_jammies.jpeg', 3, NULL, '2019-10-27 16:29:15', '2019-10-27 16:29:15'),
(12, 'jeans', 100.00, 'skinny jeans', 'images/2019-10-28-03-10-33_CeIptpOgcM_jeans.jpeg', 2, NULL, '2019-10-27 19:10:33', '2019-10-27 19:10:33'),
(13, 'bag', 2000.00, 'leather bag', 'images/2019-10-28-07-35-05_ZWypkuqknw_loewe.webp', 1, NULL, '2019-10-27 23:35:05', '2019-10-27 23:35:05'),
(14, 'Backpack', 1500.00, 'leather backpack', 'images/2019-10-29-00-47-09_JXjhES7b6d_backpack.webp', 1, NULL, '2019-10-28 16:47:09', '2019-10-28 16:47:09'),
(15, 'test authorization', 100.00, 'test', 'images/2019-10-30-08-37-41_3YTFT8SDWH_men_shorts.webp', 1, NULL, '2019-10-30 00:37:41', '2019-10-30 00:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'pending', NULL, NULL),
(2, 'complete', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transaction_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `payment_mode_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `created_at`, `updated_at`, `transaction_number`, `status_id`, `payment_mode_id`, `user_id`, `deleted_at`, `total`) VALUES
(1, '2019-10-28 23:31:47', '2019-10-29 21:11:01', '1ySHJtOlDD41572334307', 2, 1, 1, NULL, NULL),
(2, '2019-10-28 23:35:29', '2019-10-28 23:35:29', '1jAm7yd5lpy1572334529', 2, 1, 1, NULL, NULL),
(3, '2019-10-28 23:38:23', '2019-10-28 23:38:23', '1LhzezfOH9Z1572334703', 1, 1, 1, NULL, NULL),
(4, '2019-10-28 23:38:39', '2019-10-28 23:38:39', '1jAFVGhdSd91572334719', 1, 1, 1, NULL, NULL),
(5, '2019-10-28 23:39:10', '2019-10-28 23:39:10', '1JZ6wlTAhvc1572334750', 1, 1, 1, NULL, NULL),
(6, '2019-10-29 00:01:35', '2019-10-29 21:11:07', '1xlLjfFE8jF1572336095', 2, 1, 1, NULL, NULL),
(7, '2019-10-29 00:30:09', '2019-10-29 00:30:09', '11eOQokE7j21572337809', 1, 1, 1, NULL, NULL),
(8, '2019-10-29 00:30:58', '2019-10-29 00:30:58', '1NNzVxbALCi1572337858', 1, 1, 1, NULL, NULL),
(9, '2019-10-29 00:41:10', '2019-10-29 00:41:10', '1ZWBBYkaTrY1572338470', 1, 1, 1, NULL, NULL),
(10, '2019-10-29 00:53:39', '2019-10-29 00:53:39', '1v94hn59LAt1572339219', 1, 1, 1, NULL, 290.00),
(11, '2019-10-29 00:53:47', '2019-10-29 00:53:47', '1QXmbZUAlsk1572339227', 1, 1, 1, NULL, 30.00),
(12, '2019-10-29 00:54:10', '2019-10-29 00:54:10', '1DBqMRRPiVV1572339250', 1, 1, 1, NULL, NULL),
(13, '2019-10-29 00:54:17', '2019-10-29 21:11:16', '117BQTWmbwv1572339257', 2, 1, 1, NULL, 30.00),
(14, '2019-10-29 16:38:37', '2019-10-29 16:38:37', '17BQUS59Igq1572395917', 1, 1, 1, NULL, 60.00),
(15, '2019-10-29 16:40:41', '2019-10-29 16:40:41', '1mEcv880oj21572396041', 1, 1, 1, NULL, 60.00),
(16, '2019-10-29 17:50:37', '2019-10-29 17:50:37', '1pUFZHosG5h1572400237', 1, 1, 1, NULL, 300.00),
(17, '2019-10-29 17:53:13', '2019-10-29 17:53:13', '1lrIJnYHCxw1572400393', 1, 1, 1, NULL, NULL),
(18, '2019-10-29 17:53:40', '2019-10-29 17:53:40', '1HBArm8ArIt1572400420', 1, 1, 1, NULL, NULL),
(19, '2019-10-29 17:53:43', '2019-10-29 17:53:43', '1WIMF5MUkHd1572400423', 1, 1, 1, NULL, NULL),
(20, '2019-10-29 18:03:31', '2019-10-29 18:03:31', '12p0c841wfa1572401011', 1, 1, 1, NULL, 260.00),
(21, '2019-10-29 18:23:11', '2019-10-29 18:23:11', '1Ss3GEaqxJU1572402191', 1, 1, 1, NULL, 3500.00),
(22, '2019-10-29 20:08:51', '2019-10-29 21:39:51', '4Chle177iu51572408531', 1, 1, 4, NULL, 2090.00),
(23, '2019-10-29 23:33:20', '2019-10-29 23:33:20', '6xr2k2BpPHy1572420800', 1, 1, 6, NULL, 300.00),
(24, '2019-10-29 23:34:12', '2019-10-29 23:34:13', '6El6h5A7QYJ1572420852', 1, 1, 6, NULL, 130.00);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_product`
--

CREATE TABLE `transaction_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_product`
--

INSERT INTO `transaction_product` (`id`, `created_at`, `updated_at`, `quantity`, `price`, `subtotal`, `product_id`, `transaction_id`) VALUES
(1, '2019-10-28 23:39:10', '2019-10-28 23:39:10', 1, 30.00, 30.00, 11, 5),
(2, '2019-10-28 23:39:10', '2019-10-28 23:39:10', 2, 100.00, 200.00, 12, 5),
(3, '2019-10-28 23:39:10', '2019-10-28 23:39:10', 1, 200.00, 200.00, 7, 5),
(4, '2019-10-29 00:01:35', '2019-10-29 00:01:35', 1, 30.00, 30.00, 11, 6),
(5, '2019-10-29 00:01:35', '2019-10-29 00:01:35', 2, 100.00, 200.00, 12, 6),
(6, '2019-10-29 00:01:35', '2019-10-29 00:01:35', 1, 200.00, 200.00, 7, 6),
(7, '2019-10-29 00:01:35', '2019-10-29 00:01:35', 1, 2000.00, 2000.00, 13, 6),
(8, '2019-10-29 00:30:09', '2019-10-29 00:30:09', 1, 30.00, 30.00, 11, 7),
(9, '2019-10-29 00:30:09', '2019-10-29 00:30:09', 1, 200.00, 200.00, 7, 7),
(10, '2019-10-29 00:30:58', '2019-10-29 00:30:58', 1, 30.00, 30.00, 11, 8),
(11, '2019-10-29 00:30:58', '2019-10-29 00:30:58', 1, 200.00, 200.00, 7, 8),
(12, '2019-10-29 00:41:10', '2019-10-29 00:41:10', 1, 30.00, 30.00, 11, 9),
(13, '2019-10-29 00:41:10', '2019-10-29 00:41:10', 1, 200.00, 200.00, 7, 9),
(14, '2019-10-29 00:53:39', '2019-10-29 00:53:39', 3, 30.00, 90.00, 11, 10),
(15, '2019-10-29 00:53:39', '2019-10-29 00:53:39', 1, 200.00, 200.00, 7, 10),
(16, '2019-10-29 00:53:47', '2019-10-29 00:53:47', 1, 30.00, 30.00, 11, 11),
(17, '2019-10-29 00:54:17', '2019-10-29 00:54:17', 1, 30.00, 30.00, 11, 13),
(18, '2019-10-29 16:38:37', '2019-10-29 16:38:37', 2, 30.00, 60.00, 11, 14),
(19, '2019-10-29 16:40:41', '2019-10-29 16:40:41', 2, 30.00, 60.00, 11, 15),
(20, '2019-10-29 17:50:37', '2019-10-29 17:50:37', 1, 100.00, 100.00, 12, 16),
(21, '2019-10-29 17:50:37', '2019-10-29 17:50:37', 1, 200.00, 200.00, 7, 16),
(22, '2019-10-29 18:03:31', '2019-10-29 18:03:31', 2, 30.00, 60.00, 11, 20),
(23, '2019-10-29 18:03:31', '2019-10-29 18:03:31', 1, 200.00, 200.00, 7, 20),
(24, '2019-10-29 18:23:11', '2019-10-29 18:23:11', 1, 2000.00, 2000.00, 13, 21),
(25, '2019-10-29 18:23:11', '2019-10-29 18:23:11', 1, 1500.00, 1500.00, 14, 21),
(26, '2019-10-29 20:08:51', '2019-10-29 20:08:51', 3, 30.00, 90.00, 11, 22),
(27, '2019-10-29 20:08:51', '2019-10-29 20:08:51', 1, 2000.00, 2000.00, 13, 22),
(28, '2019-10-29 23:33:20', '2019-10-29 23:33:20', 1, 100.00, 100.00, 12, 23),
(29, '2019-10-29 23:33:20', '2019-10-29 23:33:20', 1, 200.00, 200.00, 7, 23),
(30, '2019-10-29 23:34:12', '2019-10-29 23:34:12', 1, 30.00, 30.00, 11, 24),
(31, '2019-10-29 23:34:12', '2019-10-29 23:34:12', 1, 100.00, 100.00, 12, 24);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '2',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'lizchuang@gmail.com', NULL, '$2y$10$7g6v8ZU3ijDEvGrTVSMeEOZVXflBK/e0RZfBMxiWbBgjEEoibXgr2', NULL, 1, NULL, '2019-10-24 17:54:42', '2019-10-24 17:54:42'),
(2, 'admin', 'liz@gmail.com', NULL, '$2y$10$05h4MaLrcid9yV5uhoHj2ezG6CrPC2o0KoET90RBUZp/kPnr4gJoq', NULL, 1, NULL, '2019-10-29 16:37:07', '2019-10-29 16:37:07'),
(3, 'liz', 'lizcc@gmail.com', NULL, '$2y$10$WHkadHsoOtlIbbsG20FZ4uuf7rYcclcTFEvtcqGMxhdvNEFSGTJ8C', NULL, 2, NULL, '2019-10-29 17:59:28', '2019-10-29 17:59:28'),
(4, 'mac', 'macmac@gmail.com', NULL, '$2y$10$EPcuKD75dsjzb1sytKzHLOPkzrsomu7Iq9/izG53xeDq8Z/SWDgfq', NULL, 2, NULL, '2019-10-29 20:08:24', '2019-10-29 20:08:24'),
(5, 'sophie', 'sophie@email.com', NULL, '$2y$10$BWtQMLhMgHR0jX8lX8FSs.CqWEAnWwQkEe6/w59AX2kvSrK1/7kr.', NULL, 2, NULL, '2019-10-29 22:45:26', '2019-10-29 22:45:26'),
(6, 'philip', 'philip@email.com', NULL, '$2y$10$feRZsGDLWGj3lkqDxbfZnOzjJkHXSHDI52soxTRdr5x0/WFxTAooy', NULL, 2, NULL, '2019-10-29 22:51:12', '2019-10-29 22:51:12'),
(7, 'liz', 'liz@yahoo.com', NULL, '$2y$10$MeFViXCNwWuF6vvj3PEGlOKL8RIGENTppfo2g5M1OK3IheRzdsxne', NULL, 2, NULL, '2019-10-29 23:52:04', '2019-10-29 23:52:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_status_id_foreign` (`status_id`),
  ADD KEY `transactions_payment_mode_id_foreign` (`payment_mode_id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`);

--
-- Indexes for table `transaction_product`
--
ALTER TABLE `transaction_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaction_product_product_id_foreign` (`product_id`),
  ADD KEY `transaction_product_transaction_id_foreign` (`transaction_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `transaction_product`
--
ALTER TABLE `transaction_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_payment_mode_id_foreign` FOREIGN KEY (`payment_mode_id`) REFERENCES `payment_modes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_product`
--
ALTER TABLE `transaction_product`
  ADD CONSTRAINT `transaction_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transaction_product_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
