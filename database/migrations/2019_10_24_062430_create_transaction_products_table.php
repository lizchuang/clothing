<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //quantity
            $table->integer('quantity');
            //price
            $table->float('price');
            //subtotal
            $table->float('subtotal');

            //product id
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            //transaction_id
            $table->unsignedBigInteger('transaction_id');
            $table->foreign('transaction_id')
                  ->references('id')->on('transactions')
                  ->onDelete('restrict')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_products');
    }
}
