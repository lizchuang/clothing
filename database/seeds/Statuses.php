<?php

use Illuminate\Database\Seeder;

class Statuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        	'name'=>'pending'
        ]) ;

        DB::table('statuses')->insert([
        	'name' => 'complete'
        ]);
    }
}
