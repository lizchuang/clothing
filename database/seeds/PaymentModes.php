<?php

use Illuminate\Database\Seeder;

class PaymentModes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_modes')->insert([
        	'name'=>'cod'
        ]) ;

        DB::table('payment_modes')->insert([
        	'name' => 'paypal'
        ]);
    }
}
