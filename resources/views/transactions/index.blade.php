@extends('layouts.app')
@section('content')

<div class="conrtainer">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			{{-- start of accordion --}}
			<div class="accordion" id="accordionExample">

				@foreach($transactions as $transaction)
				<div class="card shadow">
					<div class="card-header" id="headingOne">
						<h2 class="mb-0">
						<button class="btn btn-link w-100" type="button" data-toggle="collapse" data-target="#transaction-{{$transaction->id}}" aria-expanded="false" aria-controls="collapseOne">
						{{ $transaction->transaction_number}} / {{$transaction->created_at->format('F d,Y -h:i:s')}} 
								@if(Session::has('updated_transaction') && Session::get('updated_transaction') == $transaction->id)

									<span class="badge badge-info"> Status Updated </span>
								@endif

								@if($transaction->status->name == "pending")
								<span class="badge badge-warning float-right">
									{{$transaction->status->name}}
								</span>
								@elseif($transaction->status->name == "complete")
								<span class="badge badge-success float-right">
									{{$transaction->status->name}}
								</span>
								@endif
						</button>
						</h2>
					</div>
					<div id="transaction-{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							{{-- start of card body --}}
							<h5 class="card-title text-center">Summary</h5>
							<div class="table-responsive mb-3">

								{{-- start of table --}}
								<table class="table table-sm table-borderless">
						<tbody>
							{{-- <tr>
								<img src="{{url('../../public/images'.$product->image) }}" >
							</tr> --}}
							<tr>
								<td>Customer Name:</td>
								<td><strong>{{ $transaction->user->name}}</strong></td>
							</tr>
							<tr>
								<td>Transaction Number:</td>
								<td><strong>{{ strtoupper($transaction->transaction_number)}}</strong></td>
							</tr>
							<tr>
								<td>Mode of payment:</td>
								<td>{{ $transaction->payment_mode->name}}</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>{{ $transaction->status->name}}

									@cannot('isUser')
									<form action="{{ route('transactions.update',['transaction'=>$transaction->id])}}" method="POST" class="p-3 bg-secondary rounded">
										@csrf
										@method('PUT')
										<label for="edit-transaction-{{$transaction->id}}">Change Status</label>
										<select class="custom-select mb-1" id="edit-transaction-{{$transaction->id}}" name="status">

											@foreach($statuses as $status)
											<option value="{{$status->id}}"
												{{-- @if($status->id == $transaction->status_id)
													selected
													@endif --}} 
													{{--  Same --}}

												{{$status->id == $transaction->status_id ? "selected" : ""}}
												>{{$status->name}}
											</option>
											@endforeach

										</select>

										<button class="btn btn-primary w-180">Change Status</button>

									</form>
									@endcannot

								</td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>{{$transaction->created_at->format('F d,Y')}}</td>
							</tr>

							<tr>
								<td>Total:</td>
								<td>
									&#8369;
									{{ number_format($transaction->total,2)}}
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="2">
									<a href="{{route('transactions.show',['transaction'=>$transaction->id])}}" class="btn btn-primary">View Details</a>
								</td>
							</tr>
						</tfoot>
					</table>

								{{-- end of table --}}
							</div>

							{{-- end of card body --}}
						</div>
					</div>
				</div> {{-- end of card --}}

				@endforeach
				
			</div>
			{{-- end of accordion --}}
			
		</div>
	</div>
</div>
@endsection