@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">Transaction Details</h3>
			</div>
		</div> {{-- end of row --}}

		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="tableble-responsive">
					<table class="table table-sm table-borderless">
						<tbody>
							<tr>
								<td>Customer Name:</td>
								<td><strong>{{ $transaction->user->name}}</strong></td>
							</tr>
							<tr>
								<td>Transaction Number:</td>
								<td><strong>{{ strtoupper($transaction->transaction_number)}}</strong></td>
							</tr>
							<tr>
								<td>Mode of payment:</td>
								<td>{{ $transaction->payment_mode->name}}</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>{{ $transaction->status->name}}</td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>{{$transaction->created_at->format('F d,Y')}}</td>
							</tr>
						</tbody>
					</table>

					<table class="table">
						<thead>
							<th scope="col">Name:</th>
							<th scope="col">Quantity:</th>
							<th scope="col">Unit Price:</th>
							<th scope="col">Amount:</th>

						</thead>
						<tbody>
							@foreach($transaction->products as $itemsintransaction)
						

							<tr> {{-- per product --}}
								<td>{{$itemsintransaction->name }}</td>
								<td>{{$itemsintransaction->pivot->quantity }}</td>
								<td>&#8369; <span>{{number_format($itemsintransaction->pivot->price,2) }}</span></td>
								<td>&#8369; <span>{{number_format($itemsintransaction->pivot->subtotal,2) }}</span></td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th scope="row" colspan="3" class="text-right">Total:</th>
								<td>&#8369; <span>{{number_format($transaction->total,2) }}</span></td>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>
	</div>
{{-- {{dd($transaction_item)}} --}}


@endsection