@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center">
			Add Product
			</h3>
			<hr>
			<form action=" {{ route('products.store')}}" method="POST" enctype="multipart/form-data">
				@csrf
				
				{{-- input for name --}}
				<div class="form-group">
					<label for="name">Product name:</label>
					<input type="text" name="name" class="form-control" id="name">
				</div>
				@if($errors->has('name'))
				<div class="alert alert-danger">
					<small class="mb-0">Product name required</small>
				</div>
				@endif

				{{-- input for price--}}
				<div class="form-group">
					<label for="price">Product price:</label>
					<input
					type="number"
					name="price"
					class="form-control"
					id="price"
					min="1"
					>
				</div>
				@if($errors->has('price'))
				<div class="alert alert-danger">
					<small class="mb-0">Product price required</small>
				</div>
				@endif

				{{-- input for categor --}}
				<div class="form-group">
					<label for="category">Product Category:</label>
					<select class="form-control" id="category" name="category">
						@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->name}}
						</option>
						@endforeach
					</select>
				</div>
				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category required</small>
				</div>
				@endif

				{{-- input for image--}}
				<div class="form-group">
					<label for="image">Product image:</label>
					<input
					type="file"
					name="image"
					class="form-control-file"
					id="image"
					min="1"
					>
				</div>
				@if($errors->has('price'))
				<div class="alert alert-danger">
					<small class="mb-0">Product image required: Check if image is not greater than 2 MB</small>
				</div>
				@endif

				{{-- input for description--}}
				<div class="form-group">
					<label for="description">Product description:</label>
					<textarea
					id="description"
					class="form-control"
					cols="30"
					rows="10"
					name="description"></textarea>
				</div>
				@if($errors->has('description'))
				<div class="alert alert-danger">
					<small class="mb-0">Product description required</small>
				</div>
				@endif

				<div>
					<button class="btn btn-primary btn-block" > Add Item</button>
				</div>
			</form> {{-- add product form --}}
		</div> {{-- end of add product form --}}

		{{-- clumn for add category form --}}

		<div>
			<div class="col-12 col-md-12 mx-auto">
				@if(Session::has('category_message')) {{-- true or false --}}
					<div class="alert alert-success">
						{{ Session::get('category_message')}} {{-- GET displays the session --}}
					</div>
				@endif
				@if($errors->has('add-category'))
					<div class="alert alert-danger">
						Category not added
					</div>
				@endif
				
				<form method="POST" action="{{route('categories.store')}}">
					@csrf
					<div class="form-group">
						<label for="add-category">Add category:</label>
						<input type="text" name="add-category" id="add-category" class="form-control">
					</div>

					<button class="btn btn-secondary w-100">Add category</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection