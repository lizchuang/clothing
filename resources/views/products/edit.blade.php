@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center">
			Edit Product
			</h3>
			<hr>
			@if (Session::has('update_failed'))
				<div class="alert alert-warning">
					{{ Session::get('update_failed')}}
				</div>
			@endif
			@if (Session::has('update_success'))
				<div class="alert alert-success">
					{{ Session::get('update_success')}}
				</div>
			@endif
			<form action=" {{ route('products.update',['product'=> $product->id])}}" method="POST" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				
				{{-- input for name --}}
				<div class="form-group">
					<label for="name">Product name:</label>
					<input type="text" name="name" class="form-control" id="name" class="form-control" value ="{{$product->name}}">
				</div>
				@if($errors->has('name'))
				<div class="alert alert-danger">
					<small class="mb-0">Product name required</small>
				</div>
				@endif
				{{-- input for price--}}
				<div class="form-group">
					<label for="price">Product price:</label>
					<input
					type="number"
					name="price"
					class="form-control"
					id="price"
					min="1"
					value ="{{$product->price}}"
					>
				</div>
				@if($errors->has('price'))
				<div class="alert alert-danger">
					<small class="mb-0">Product price required</small>
				</div>
				@endif
				{{-- input for category --}}
				<div class="form-group">
					<label for="category">Product Category:</label>
					<select class="form-control" id="category" name="category">
						@foreach($categories as $category)
						<option
							value="{{$category->id}}"
							@if($product->category_id == $category->id)
							selected
							@endif>{{$category->name}}
						</option>
						@endforeach
					</select>
				</div>
				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category required</small>
				</div>
				@endif
				{{-- input for image--}}
				<div class="form-group">
					<label for="image">Product image:</label>
					<input
					type="file"
					name="image"
					class="form-control-file"
					id="image"
					min="1"
					>
				</div>
				@if($errors->has('price'))
				<div class="alert alert-danger">
					<small class="mb-0">Product image required: Check if image is not greater than 2 MB</small>
				</div>
				@endif
				{{-- input for description--}}
				<div class="form-group">
					<label for="description">Product description:</label>
					<textarea
					id="description"
					class="form-control"
					cols="30"
					rows="10"
					name="description">{{$product->description}}</textarea>
				</div>
				@if($errors->has('description'))
				<div class="alert alert-danger">
					<small class="mb-0">Product description required</small>
				</div>
				@endif
				<div>
					<button class="btn btn-primary btn-block" >Edit Item</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection