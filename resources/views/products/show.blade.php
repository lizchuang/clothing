@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					View Product
				</h3>
				<hr>

				<div class="card shadow">
					<img src="{{url('/public/'.$product->image) }}" 
					class="card-img-top single">
					<div class="card-body">
						<div class="card-foot">

							@cannot('isAdmin')
							<form action="{{route('carts.store')}}" method="post" class="add-to-cart-field">
									<div class="form-group">
										@csrf
										<input type="hidden" name="id" value="{{$product->id}}">
										<label for="quantity">Quantity:</label>

										{{-- Start of append button --}}
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-outline-secondary deduct-quantity" type="button" data-id="{{$product->id}}">-</button>
											</div>

											<input type="number" name="quantity" class="form-control input-quantity" data-id="{{$product->id}}" placeholder="Enter quantity">

											<div class="input-group-append">
												<button class="btn btn-outline-secondary add-quantity" type="button" data-id="{{$product->id}}">+</button>
											</div>
										</div>
										{{-- End of append button --}}
									</div>
								
									<button  type="submit" class="btn btn-success my-1 w-100">Add to Cart
									</button>
								</form>
								@endcannot
								
							{{-- View button --}}
							{{-- <a 
								href="{{route('products.show',['product' =>$product->id])}}" 
								class="btn btn-primary w-100 my-1">View Product
							</a>
 --}}
							{{-- Edit button --}}
							{{-- @cannot('isUser') --}}
							@can('isAdmin')
							<a 
								href="{{route('products.edit',['product'=> $product->id])}}" 
								class="w-100 btn btn-warning my-1">Edit Product
							</a>

							{{-- Delete form --}}
							<form action="{{route('products.destroy',['product' =>$product->id])}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-warning w-100 my-1"> Delete Product</button>
								
							</form>
							@endcan
							{{-- @endcannot --}}
							
							
						</div>
						<h2 class="card-title">
						{{$product->name}}
						</h2>

						<p class="card-text">
							<strong>
								&#8369 {{$product->price}}
							</strong>
						</p>
						<p class="card-text">
							{{$product->description}}
						</p>
					</div>
				</div> 
				{{-- endof card --}}

			</div>
		</div>
	</div>

@endsection