@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3 class="text-center">
				My Cart Details
			</h3>
		</div>
		<!--  -->
	</div> {{-- endrow --}}
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			@if((Session::has('cart')) && count(Session::get('cart')))
			<div class="table-responsive">
				<table class="table table-striped table-hover text-center">
					<thead>
						<tr>
							<th>Name</th>
							<th>Unit Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{{-- @foreach() --}}
						{{-- {{dd(Session::get('cart'))}} --}}
						{{-- 	{{dd($products)}} --}}
						@foreach($products as $product)
						<tr>
							<th scope="row">{{$product->name}}</th>
							<td> &#8369; <span class="product-price">{{$product->price}}</span> </td>
							<td>
								<form action="{{route('carts.store')}}" method="post" class="add-to-cart-field">
									<div class="form-group">
										@csrf
										<input type="hidden" name="id" value="{{$product->id}}">
										<label for="quantity">Quantity:</label>
										{{-- Start of append button --}}
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-outline-secondary deduct-quantity" type="button" data-id="{{$product->id}}">-</button>
											</div>
											<input type="number" name="quantity" class="form-control input-quantity" data-id="{{$product->id}}"  value="{{$product->quantity}}">
											<div class="input-group-append">
												<button class="btn btn-outline-secondary add-quantity" type="button" data-id="{{$product->id}}">+</button>
											</div>
										</div>
										{{-- End of append button --}}
									</div>
									
									<button  type="submit" class="btn btn-success my-1 w-80 shadow">Add to Cart
									</button>
								</form>
							</td>
							<td>
								&#8369;<span class="product-subtotal">{{$product->subtotal}}</span>
							</td>
							<td>
								<form action="{{route('carts.destroy', ['cart' => $product->id]) }}" method="POST">
									@csrf
									@method ('DELETE')
									<button class="btn btn-danger shadow">Remove from cart</button>
								</form>
							</td>
						</tr>
						
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" class="text-right">Total</td>
							<td> &#8369; <span id="total_price">{{$total}}</span></td>
							<td>
								<form action="{{route('transactions.store')}}" method="POST">
									@csrf
									<button 
									@cannot('isLogged') type="button" data-toggle="modal" data-target="#authentication-modal" @endcannot
									class="btn btn-primary">Checkout Cart</button>

								</form>
								{{-- Add paypal button --}}
								<hr>
								<p class="mb-0">Pay via</p>
								<div class="py-3" id="paypal-button-container"></div>
							</td>
						</tr>
					</tfoot>
				</table> {{-- end of table --}}
			</div> {{-- end of table responsive --}}
			<form action="{{route('carts.clear')}}" method="POST">
				@csrf
				@method('DELETE')
				<button class="btn btn-danger w-100">Empty Cart</button>
				
			</form>
			@else
			<div class="alert alert-warning text-center shadow">No items in the cart.Please visit our products using this
				<a href="{{route('products.index')}}">link</a>
			</div>
			@endif
		</div> {{-- end of column --}}
	</div>
</div>


{{-- modal --}}
<div class="modal fade" id="authentication-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Authentication required to continue</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Please log in to continue
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<a href="{{route ('login')}}" class="btn btn-primary">Login</a>
			</div>
		</div>
	</div>
</div>

<script src="https://www.paypal.com/sdk/js?client-id=AeVqbHs7PEyJiWaTSJmWJiXwRukonsweAr8nm3C1SyeAsaLqYQJnImh9V6NrEJTZICoY55bwY1FJnop6">
	
</script>

<script>
	paypal.Buttons
	({
		createOrder: function(data, actions) 
		{
			// Set up the transaction
			return actions.order.create
			({
				purchase_units: 
				[{
					amount:
					{
						value: document.querySelector('#total_price').innerHTML
					}
				}]
			});
		},

		onApprove: function(data, actions) 
		{
      // Capture the funds from the transaction
      		return actions.order.capture().then(function(details) 
      		{
        // Show a success message to your buyer
        		// alert('Transaction completed by ' + details.payer.name.given_name);
        		console.log(details.id)
        		let csrfToken = document.querySelector('meta[name= "csrf-token"]').getAttribute('content'); 
        		console.log(csrfToken)
        		fetch('http://127.0.0.1:8000/transactions/paypal/create', {
        			method: 'post',
        			body: JSON.stringify( { transactionNumber : details.id } ),
        			headers :{ 'X-CSRF-TOKEN' : csrfToken,
        						'Accept' : "application/json"
        			 }
        		})
        		.then(response => response.json())
        		.then(result => {
        			window.location = result.url 
        			// console.log(result.url)
        		});
   		 	});

  		}
	}).render('#paypal-button-container');

</script>
@endsection