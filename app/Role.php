<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class Role extends Model
{

	use SoftDeletes;
    public function users(){
    	return $this->hasMany('App\User');
    }
}
