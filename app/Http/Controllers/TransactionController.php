<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Auth;
use Session;
use Str;
use App\Product;
use App\Status;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = Status::all();

        if(Auth::user()->role_id === 1)
        {
            $transactions = Transaction::all();
        }
        else
        {
             $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        }
        // dd($transactions);
        return view('transactions.index')
             ->with('transactions',$transactions)
             ->with('statuses',$statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // flow:
        // 1.create an entry in transactions table with the ff attributes
            //transaction number->unique
            //user_id
        // 2.add an entry/entries transaction_product linked to the newly created enrtyin transaction table
        // 3. compute the total price
        // 4. update the total attribute of the newly created entry in transaction
        // 5. clear cart session

        // ================================================================

        // 1.create an entry in transactions table with the ff attributes
             $transaction =  new Transaction;
                //transaction number->unique
                     // userid+randomcharacter+time
                 $transaction_number= Auth::user()->id.Str::random(10).time();
                 //user_id
                 $user_id = Auth::user()->id;
                 
            $transaction->transaction_number = $transaction_number;
            $transaction->user_id = $user_id;
            $transaction->save();

            // 2.add an entry/entries transaction_product linked to the newly created enrtyin transaction table
                // 2.1. get the keys from the session;this contains the products the user wants to buy
                $cart_ids = array_keys(Session::get('cart'));

                // 2.2. use the aray of product id to find it in the database
                $products = Product::find($cart_ids);

                // dd(Session::get('cart'));
                //  2.3. for every products listed in the cart,add entry to the pivot table transaction product and include data(subtotal,quantity,price, product_id, and transaction_id)

                // dd($products);

                // $transaction->products()->attach(//id of reference table--product);
                // $product->transactions()->attach(//)

                foreach (Session::get('cart') as $cart_order_id => $quantity) {
                    foreach($products as $product){
                        if($product->id == $cart_order_id){
                            $transaction->products()->attach(
                                $product->id,
                                [
                                    'quantity' => $quantity,
                                    'price' => $product->price,
                                    'subtotal' => $quantity * $product->price

                                ]
                            );
                        }
                    }
                }

                // 3. compute the total price

                $total=0;
                $transaction_products = $transaction->products;
                // dd($transaction_products);
                foreach ($transaction_products as $transaction_product) {
                      $total+= $transaction_product->pivot->subtotal;
                }

                // 4. update the total attribute of the newly created entry in transaction
                $transaction->total = $total;
                $transaction->save();

                 // 5. clear cart session
                Session::forget('cart');
                return redirect(route('transactions.show',['transaction'=> $transaction->id]) );

              
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        // echo "showing single $transaction";
        $this-> authorize('view', $transaction);
        return view('transactions.show')->with('transaction',$transaction);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $transaction->status_id = $request ->input('status');
        $transaction->save();
        return redirect(route('transactions.index') )->with('updated_transaction',$transaction->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function paypal_store(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $transaction =  new Transaction;
        $transaction_number = $data['transactionNumber'];
        // $transaction_number= Auth::user()->id.Str::random(10).time();
        $user_id = Auth::user()->id;

               
        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;
        $transaction->payment_mode_id = 2;
        $transaction->save();

        // Ready data for pivot table
        $cart_ids = array_keys(Session::get('cart'));
        $products = Product::find($cart_ids);
 
        // add entry in pivot table
        foreach (Session::get('cart') as $cart_order_id => $quantity) {
            foreach($products as $product){
                if($product->id == $cart_order_id){
                    $transaction->products()->attach(
                        $product->id,
                        [
                            'quantity' => $quantity,
                            'price' => $product->price,
                            'subtotal' => $quantity * $product->price

                        ]
                    );
                }
            }
        }

                    // 3. compute the total price

        $total=0;
        $transaction_products = $transaction->products;
                    // dd($transaction_products);
        foreach ($transaction_products as $transaction_product) {
                          $total+= $transaction_product->pivot->subtotal;
        }

                    // 4. update the total attribute of the newly created entry in transaction
        $transaction->total = $total;
        $transaction->save();

                     // 5. clear cart session
        Session::forget('cart');




        return ["url"=> route('transactions.show',['transaction' => $transaction->id])];
    }
}
