<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $this->authorize('create',$product);

        $categories = Category::all();
        return view('products.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Product $product)
    {

        $this->authorize('create',$product);
        //flow
            // 1.validate each field(validation errors will display in views)
            // 2. save uploaded image (to get the path which will be save to the database)
            // 3. get input values from form
            // 4. save to details to the database
            // 5. redirect user to view the newly created product

        //====================================================================

          // 1.validate each field(validation errors will display in views)
            $request->validate([
                'name' => 'required|string',
                'price' => 'required|numeric',
                'category' =>'required',
                'image' => 'required|image|max:2000',
                'description' => 'required|string'
            ]);


            // 2. save uploaded image (to get the path which will be save to the database)

                //2.1. get uploaded file
                    $file =$request->file('image');
                    
                //2.2. get uploaded filename
                    $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                //2.3. get uploaded file extension name
                    $file_extension = $file->extension();

                //2.4. generate new name with random characters
                    $random_chars = Str::random(10);

                    $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_" .$file_name.".".$file_extension;
                    // dd($new_file_name);
                //2.5. save the new file to public folder
                    $filepath = $file->storeAs('images',$new_file_name, 'public');

            // 3. get input values from form
                    $name = $request->input('name');
                    $price = $request->input('price');
                    $category_id = $request->input('category');
                    $description = $request->input('description');
                    $image = $filepath;

            // 4. save to details to the database
                    $product = new Product;
                    $product->name = $name;
                    $product->price = $price;
                    $product->category_id = $category_id;
                    $product->description = $description;
                    $product->image = $image;

                    $product->save();


            // 5. redirect user to view the newly created product
                    return redirect(route('products.show',['product'=>$product->id]));





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)

    {
        $this->authorize('update', $product);
        $categories = Category::all();
        return view('products.edit')
            ->with('product',$product)
            ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('update', $product);
        //flow
            // 1. validate the form,check if all fields are not empty,except  image input
            // 2. upadate database if there are differences bet the details from form and the current details of the product

                // =============================================================

        // 1. validate the form,check if all fields are not empty,except  image input
                 $request->validate([
                'name' => 'required|string',
                'price' => 'required|numeric',
                'category' =>'required',
                // 'image' => 'required|image|max:2000',
                'description' => 'required|string'
            ]);

        // 2. upadate database if there are differences bet the details from form and the current details of the product

                 if(
                    !$request->hasFile('image') &&
                    $product->name == $request->input('name') &&
                    $product->description == $request->input('description') &&
                    $product->price == $request->input('price') &&
                    $product->category_id == $request->input('category')
                 )
                {
                    $request ->session()->flash('update_failed', 'Something went wrong');
                    // return redirect( route('products.edit',['product' =>$product->id]));
                }else{
                    //update the entry in the database and return the update entry

                    //check if there's a file uploaded
                    if($request->hasFile('image'))
                    {
                        $request->validate([
                            'image' => 'required|image|max:2000'
                        ]);

                    $file =$request->file('image');
                    $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $file_extension = $file->extension();
                    $random_chars = Str::random(10);

                    $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_" .$file_name.".".$file_extension;
                    
                    $filepath = $file->storeAs('images',$new_file_name, 'public');

                    //set the new image path as image prooduct
                    $product->image =$filepath;
                    }

                    //update the product attribute
                    $product->name =$request->input('name');
                    $product->price =$request->input('price');
                    $product->description =$request->input('description');
                    $product->category_id =$request->input('category');

                    $product->save();

                    $request->session()->flash('update_success','Product successfully updated');

                }

                return redirect(route('products.show',['product' => $product->id]));

 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);
        $product->delete();
        // $request->session()->flash('destroy_success','Product Destroyed');
        return redirect(route('products.index'))
        ->with('destroy_message','Product Destroyed');
    }
}
