<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('products.index'));
});

Auth::routes();

Route::get('/home', 'ProductController@index')->name('home');

Route::post('transactions/paypal/create', 'TransactionController@paypal_store')->name('paypal.store');
Route::delete('/cart/clear','CartController@clearCart')->name('carts.clear');
Route::resource('products','ProductController');
Route::resource('categories','CategoryController');
Route::resource('carts','CartController');
Route::resource('transactions', 'TransactionController');
